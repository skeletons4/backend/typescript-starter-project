# Summary

Description about bug report

## Steps to reproduce

1. Step 1
2. Step 2
3. Step 3

## Expected Behavior

A long description about expected behavior

## Seen Behavior

A long description about seen behavior

## Screenshots

Screenshots for bug representation
Charge images here!

## External Links

- [Links related to bug]()

## Assets

If exist assets related to bug
Charge assets here!